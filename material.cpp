#include "material.h"
#include "sphere.h"
#include "mesh.h"
#include "Texture.h"
#include "point_light.h"

namespace fst {
    Material::Material(const math::Vector3f &ambient, const math::Vector3f &diffuse, const math::Vector3f &specular,
                       const math::Vector3f &mirror, float phong_exponent)
            : m_ambient(ambient), m_diffuse(diffuse), m_specular(specular), m_mirror(mirror),
              m_phong_exponent(phong_exponent) {}

    std::pair<math::Vector3f, char>
    Material::computeBrdf(const math::Vector3f &wi, const math::Vector3f &wo, const math::Vector3f &normal,
                          HitRecord &hit_record,
                          math::Vector3f radiance) const {
        auto diffuse = math::max(math::dot(normal, wi), 0.0f);
        auto specular = std::pow(math::max(math::dot(math::normalize(wo + wi), normal), 0.0f), m_phong_exponent);


        Texture textureHandler;
        std::pair<math::Vector3f, char> result;
        if (hit_record.type == 1) {
            auto hitObj = (fst::Sphere *) hit_record.hitObj;
            if (hitObj->parserObj->texturePtr) {
                math::Vector3f color = textureHandler.textureCalculator(hit_record);
                if (hitObj->parserObj->texturePtr->decalMode == 'r') {
                    result.first = (m_specular * specular + color / 255 * diffuse) * radiance;
                    result.second = 1;
                    return result;
                }
                if (hitObj->parserObj->texturePtr->decalMode == 'b') {
                    result.first = (m_specular * specular + (m_diffuse + color / 255) / 2 * diffuse) * radiance;
                    result.second = 1;
                    return result;
                }
                if (hitObj->parserObj->texturePtr->decalMode == 'a') {
                    result.first = color;
                    result.second = 0;
                    return result;
                }
            }
        } else if (hit_record.type == 2) {
            auto hitObj = (fst::Triangle *) hit_record.hitObj;
            if (hitObj->parserObj->texturePtr) {
                math::Vector3f color = textureHandler.textureCalculator(hit_record);
                if (hitObj->parserObj->texturePtr->decalMode == 'r') {
                    result.first = (m_specular * specular + color / 255 * diffuse) * radiance;
                    result.second = 1;
                    return result;
                }
                if (hitObj->parserObj->texturePtr->decalMode == 'b') {
                    result.first = (m_specular * specular + (m_diffuse + color / 255) / 2 * diffuse) * radiance;
                    result.second = 1;
                    return result;
                }
                if (hitObj->parserObj->texturePtr->decalMode == 'a') {
                    result.first = color;
                    result.second = 0;
                    return result;
                }
            }
        }

        result.first = (m_specular * specular + m_diffuse * diffuse) * radiance;
        result.second = 1;
        return result;
    }
}