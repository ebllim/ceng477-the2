#ifndef __SCENE_H__
#define __SCENE_H__

#include "camera.h"
#include "material.h"
#include "mesh.h"
#include "parser.h"
#include "point_light.h"
#include "ray.h"
#include "sphere.h"
#include "vector3f.h"

#include <vector>

namespace fst
{
    struct Scene
    {
        //Data
        std::vector<Camera> cameras;
        std::vector<PointLight> point_lights;
        std::vector<Material> materials;
        std::vector<math::Vector3f> vertex_data;
        std::vector<Mesh> meshes;
        std::vector<Sphere> spheres;
        math::Vector3f background_color;
        math::Vector3f ambient_light;
        float shadow_ray_epsilon;
        int max_recursion_depth;
        std::vector<unsigned char*> textureJpgs;

        //Functions
        void loadFromParser(parser::Scene &parser);
        bool intersect(const Ray& ray, HitRecord& hit_record, float max_distance) const;
        bool intersectShadowRay(const Ray& ray, float max_distance) const;
    };
}
#endif