#ifndef __HIT_RECORD__
#define __HIT_RECORD__

#include "vector3f.h"

namespace fst
{
    struct HitRecord
    {
        math::Vector3f normal;
        float distance;
        int material_id;
        void *hitObj;
        char type;
        float w1;
        float w2;
        math::Vector3f hitPoint;
    };
}
#endif