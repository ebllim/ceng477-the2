#include <cmath>
#include <cstring>
#include "Texture.h"
#include "sphere.h"
#include "mesh.h"
#include "fst_globals.h"

Texture::Texture() {
}

#define FETCH(X, Y, I)  textureImg[3 * (nx * Y + X) + I]
//#define FETCH(X, Y, I)  textureImg[3 * (Y * nx + X) + I]
// Hocam şu üstteki yüzünden 1.5 gün aptal aptal gezdik. ne farkları var ki

float SafeAcos(float x) {
    if (x < -1.0) x = -1.0;
    else if (x > 1.0) x = 1.0;
    return static_cast<float>(acos(x));
}

extern fst::math::Vector4f operator*(const fst::math::Matrix4f m, const fst::math::Vector4f &point);

fst::math::Vector3f Texture::textureCalculator(fst::HitRecord &hit_record) {
    float u = 0, v = 0;
    int nx = 0, ny = 0;
    unsigned char *textureImg = nullptr;
    char interpolation = 0, appearance = 0;

    if (hit_record.type == 1) {
        auto hitObj = (fst::Sphere *) hit_record.hitObj;

        fst::math::Matrix4f totalRotation;
        totalRotation.toIdentity();
        for(auto& transformation: hitObj->parserObj->transformations){
            if('r' == transformation.first){
                parser::Rotation rotation = g_scene.transformations.rotations[transformation.second - 1];
                totalRotation = totalRotation * rotation.matrix;
            }
        }

        totalRotation = fst::math::transpose(totalRotation);
        textureImg = hitObj->parserObj->texturePtr->textureImg;
        interpolation = hitObj->parserObj->texturePtr->interpolation;
        appearance = hitObj->parserObj->texturePtr->appearance;


        parser::Vec3f &center = hitObj->parserObj->center;
        fst::math::Vector4f intersectionPoint = fst::math::Vector4f(hit_record.hitPoint.x - center.x, hit_record.hitPoint.y - center.y, hit_record.hitPoint.z - center.z, 1);
        intersectionPoint = totalRotation * intersectionPoint;

        float thetha = SafeAcos((intersectionPoint.y) / hitObj->parserObj->radius);
        float phi = atan2((intersectionPoint.z), (intersectionPoint.x));
//        float thetha = SafeAcos((hit_record.hitPoint.y - center.y) / hitObj->parserObj->radius);
//        float phi = atan2((hit_record.hitPoint.z - center.z), (hit_record.hitPoint.x - center.x));

        u = static_cast<float>((-phi + PI) / (2 * PI));
        v = static_cast<float>(thetha / PI);
        nx = hitObj->parserObj->texturePtr->nx;
        ny = hitObj->parserObj->texturePtr->ny;


    } else if (hit_record.type == 2) {
        auto hitObj = (fst::Triangle *) hit_record.hitObj;
        textureImg = hitObj->parserObj->texturePtr->textureImg;
        interpolation = hitObj->parserObj->texturePtr->interpolation;
        appearance = hitObj->parserObj->texturePtr->appearance;

        auto w1 = hit_record.w1;
        auto w2 = hit_record.w2;

        float ua = hitObj->parserObj->texCoord0.x;
        float ub = hitObj->parserObj->texCoord1.x;
        float uc = hitObj->parserObj->texCoord2.x;

        float va = hitObj->parserObj->texCoord0.y;
        float vb = hitObj->parserObj->texCoord1.y;
        float vc = hitObj->parserObj->texCoord2.y;

        u = ua + w1 * (ub - ua) + w2 * (uc - ua);
        v = va + w1 * (vb - va) + w2 * (vc - va);

        nx = hitObj->parserObj->texturePtr->nx;
        ny = hitObj->parserObj->texturePtr->ny;

    }


    int int_i, int_j;
    float i, j;

    if ('c' == appearance) {
        if (u > 1) u = 1;
        else if (u < 0) u = 0;
        if (v > 1) v = 1;
        else if (v < 0) v = 0;

    } else if (appearance == 'r') {
        u = u - std::floor(u);
        v = v - std::floor(v);
    }

    i = u * nx;
    j = v * ny;
    fst::math::Vector3f color{};
    if (interpolation == 'b') {
        int p = floor(i);
        int q = floor(j);
        float dx = i - p;
        float dy = j - q;

        color.x = FETCH(p, q, 0) * (1 - dx) * (1 - dy)
                  + FETCH(p + 1, q, 0) * (dx) * (1 - dy)
                  + FETCH(p, q + 1, 0) * (1 - dx) * (dy)
                  + FETCH(p + 1, q + 1, 0) * (dx) * (dy);

        color.y = FETCH(p, q, 1) * (1 - dx) * (1 - dy)
                  + FETCH(p + 1, q, 1) * (dx) * (1 - dy)
                  + FETCH(p, q + 1, 1) * (1 - dx) * (dy)
                  + FETCH(p + 1, q + 1, 1) * (dx) * (dy);

        color.z = FETCH(p, q, 2) * (1 - dx) * (1 - dy)
                  + FETCH(p + 1, q, 2) * (dx) * (1 - dy)
                  + FETCH(p, q + 1, 2) * (1 - dx) * (dy)
                  + FETCH(p + 1, q + 1, 2) * (dx) * (dy);

    } else if (interpolation == 'n') {
        int_i = (int) std::round(i);
        int_j = (int) std::round(j);
        color.x = FETCH(int_i, int_j, 0);
        color.y = FETCH(int_i, int_j, 1);
        color.z = FETCH(int_i, int_j, 2);
    }


    return color;
}


void loadTextureJpegs(std::vector<unsigned char *> &textureJpgs, std::vector<parser::Texture> &textures) {
    int w, h;
    for (auto &texture : textures) {
        unsigned char *tmpJpg;

        char *s = new char[texture.image_name.length() + 1];
        std::strcpy(s, texture.image_name.c_str());

        read_jpeg_header(s, w, h);
        tmpJpg = new unsigned char[w * h * 3];
        read_jpeg(s, tmpJpg, w, h);


        textureJpgs.push_back(tmpJpg);
        texture.textureImg = tmpJpg;
        texture.nx = w;
        texture.ny = h;
    }

}
