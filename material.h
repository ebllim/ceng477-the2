#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <utility>
#include "hit_record.h"
#include "vector3f.h"
#include "point_light.h"

namespace fst
{
    class Material
    {
    public:
        Material(const math::Vector3f& ambient, const math::Vector3f& diffuse, const math::Vector3f& specular, const math::Vector3f& mirror, float phong_exponent);

        std::pair<math::Vector3f, char>
        computeBrdf(const math::Vector3f &wi, const math::Vector3f &wo, const math::Vector3f &normal,
                    HitRecord &hit_record,
                    math::Vector3f radiance) const;

        const math::Vector3f& get_ambient() const { return m_ambient; }
        const math::Vector3f& get_mirror() const { return m_mirror; }

    private:
        math::Vector3f m_ambient;
        math::Vector3f m_diffuse;
        math::Vector3f m_specular;
        math::Vector3f m_mirror;
        float m_phong_exponent;
    };
}
#endif