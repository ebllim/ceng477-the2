#ifndef __INTEGRATOR_H__
#define __INTEGRATOR_H__

#include "scene.h"

namespace fst
{
    class Integrator
    {
    public:
        Integrator(parser::Scene &parser);

        math::Vector3f renderPixel(const Ray& ray, int depth) const;
        void integrate() const;

    private:
        Scene m_scene;
    };
}
#endif