#include "parser.h"
#include "integrator.h"
#include "timer.h"
#include "matrix4f.h"
#include "fst_globals.h"

#include <iostream>
#include <iomanip>

parser::Scene g_scene;
int main(int argc, char *argv[]) {

    fst::Timer timer;
    fst::Timer total_timer;

    total_timer.start();
    timer.start();
    g_scene.loadFromXml(argv[1]);
    std::cout << std::fixed << std::showpoint << std::setprecision(5);
    std::cout << "loadFromXml    : " << timer.getTime() << " seconds" << std::endl;

    timer.start();
    g_scene.meshTransformation();
    std::cout << "Mesh Transform : " << timer.getTime() << " seconds" << std::endl;

    timer.start();
    g_scene.triangleTransformation();
    std::cout << "Triangle Transform : " << timer.getTime() << " seconds" << std::endl;

    timer.start();
    g_scene.spheresTransformations();
    std::cout << "spheresTransformations : " << timer.getTime() << " seconds" << std::endl;

    timer.start();
    g_scene.meshInstancesTransformation();
    std::cout << "Mesh Instance Transform : " << timer.getTime() << " seconds" << std::endl;

    timer.start();
    fst::Integrator integrator(g_scene);
    std::cout << "loadFromParser : " << timer.getTime() << " seconds" << std::endl;

    timer.start();
    integrator.integrate();
    std::cout << "Traversal      : " << timer.getTime() << " seconds" << std::endl;
    std::cout << std::endl << "Total          : " << total_timer.getTime() << " seconds" << std::endl;

    return 0;
}
