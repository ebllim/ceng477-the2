all:
	g++ -std=c++11 -O3  *.h *.cpp -ljpeg -pthread -o raytracer

clean:
	rm -f raytracer
	rm -f *.ppm

test_all: test_simple_transform test_skybox test_spheres_transform test_horse test_horse_instanced

test_simple_transform:
	./raytracer hw2_support_files/hw2_scenes/simple_transform.xml

test_horse:
	./raytracer hw2_support_files/hw2_scenes/horse.xml

test_horse_instanced:
	./raytracer hw2_support_files/hw2_scenes/horse_instanced.xml

test_skybox:
	./raytracer hw2_support_files/hw2_scenes/skybox.xml

test_spheres_transform:
	./raytracer hw2_support_files/hw2_scenes/spheres_transform.xml

old_make:
	g++ -O3 *.cpp -c -std=c++11 -march=native
	g++ *.o -o raytracer -lpthread -ljpeg

old_clean:
	rm -f *.o
