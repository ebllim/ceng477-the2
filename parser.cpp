#include "parser.h"
#include "tinyxml2.h"
#include "vector3f.h"
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <cmath>


void parser::Scene::loadFromXml(const std::string &filepath) {
    tinyxml2::XMLDocument file;
    std::stringstream stream;

    auto res = file.LoadFile(filepath.c_str());
    if (res) {
        throw std::runtime_error("Error: The xml file cannot be loaded.");
    }

    auto root = file.FirstChild();
    if (!root) {
        throw std::runtime_error("Error: Root is not found.");
    }

    //Get BackgroundColor
    auto element = root->FirstChildElement("BackgroundColor");
    if (element) {
        stream << element->GetText() << std::endl;
    } else {
        stream << "0 0 0" << std::endl;
    }
    stream >> background_color.x >> background_color.y >> background_color.z;

    //Get ShadowRayEpsilon
    element = root->FirstChildElement("ShadowRayEpsilon");
    if (element) {
        stream << element->GetText() << std::endl;
    } else {
        stream << "0.001" << std::endl;
    }
    stream >> shadow_ray_epsilon;

    //Get MaxRecursionDepth
    element = root->FirstChildElement("MaxRecursionDepth");
    if (element) {
        stream << element->GetText() << std::endl;
    } else {
        stream << "0" << std::endl;
    }
    stream >> max_recursion_depth;

    //Get Cameras
    element = root->FirstChildElement("Cameras");
    element = element->FirstChildElement("Camera");
    Camera camera;
    while (element) {
        auto child = element->FirstChildElement("Position");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("Gaze");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("Up");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("NearPlane");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("NearDistance");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("ImageResolution");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("ImageName");
        stream << child->GetText() << std::endl;

        stream >> camera.position.x >> camera.position.y >> camera.position.z;
        stream >> camera.gaze.x >> camera.gaze.y >> camera.gaze.z;
        stream >> camera.up.x >> camera.up.y >> camera.up.z;
        stream >> camera.near_plane.x >> camera.near_plane.y >> camera.near_plane.z >> camera.near_plane.w;
        stream >> camera.near_distance;
        stream >> camera.image_width >> camera.image_height;
        stream >> camera.image_name;

        cameras.push_back(camera);
        element = element->NextSiblingElement("Camera");
    }

    //Get Lights
    element = root->FirstChildElement("Lights");
    auto child = element->FirstChildElement("AmbientLight");
    stream << child->GetText() << std::endl;
    stream >> ambient_light.x >> ambient_light.y >> ambient_light.z;
    element = element->FirstChildElement("PointLight");
    PointLight point_light;
    while (element) {
        child = element->FirstChildElement("Position");
        stream << child->GetText() << std::endl;
        child = element->FirstChildElement("Intensity");
        stream << child->GetText() << std::endl;

        stream >> point_light.position.x >> point_light.position.y >> point_light.position.z;
        stream >> point_light.intensity.x >> point_light.intensity.y >> point_light.intensity.z;

        point_lights.push_back(point_light);
        element = element->NextSiblingElement("PointLight");
    }

    //Get Materials
    element = root->FirstChildElement("Materials");
    element = element->FirstChildElement("Material");
    Material material;
    while (element) {
        child = element->FirstChildElement("AmbientReflectance");
        stream << (child ? child->GetText() : "0 0 0") << std::endl;
        child = element->FirstChildElement("DiffuseReflectance");
        stream << (child ? child->GetText() : "0 0 0") << std::endl;
        child = element->FirstChildElement("SpecularReflectance");
        stream << (child ? child->GetText() : "0 0 0") << std::endl;
        child = element->FirstChildElement("MirrorReflectance");
        stream << (child ? child->GetText() : "0 0 0") << std::endl;

        child = element->FirstChildElement("PhongExponent");
        stream << (child ? child->GetText() : "0") << std::endl;

        stream >> material.ambient.x >> material.ambient.y >> material.ambient.z;
        stream >> material.diffuse.x >> material.diffuse.y >> material.diffuse.z;
        stream >> material.specular.x >> material.specular.y >> material.specular.z;
        stream >> material.mirror.x >> material.mirror.y >> material.mirror.z;
        stream >> material.phong_exponent;

        materials.push_back(material);
        element = element->NextSiblingElement("Material");
    }

    //Get VertexData
    element = root->FirstChildElement("VertexData");
    stream << element->GetText() << std::endl;
    Vec3f vertex;
    while (!(stream >> vertex.x).eof()) {
        stream >> vertex.y >> vertex.z;
        vertex_data.push_back(vertex);
    }
    stream.clear();

    //Get Textures
    element = root->FirstChildElement("Textures");
    if (element) {
        element = element->FirstChildElement("Texture");
        Texture texture;
        std::string tmpStr;

        while (element) {
            child = element->FirstChildElement("ImageName");
            stream << child->GetText() << std::endl;
            stream >> texture.image_name;

            child = element->FirstChildElement("Interpolation");
            stream << child->GetText() << std::endl;
            stream >> texture.interpolation;
            stream.str("");
            stream.clear();

            child = element->FirstChildElement("DecalMode");
            stream << child->GetText() << std::endl;
            stream >> tmpStr;
            if (tmpStr == "replace_kd") {
                texture.decalMode = 'r';
            } else if (tmpStr == "blend_kd") {
                texture.decalMode = 'b';
            } else if (tmpStr == "replace_all") {
                texture.decalMode = 'a';
            } else {
                texture.decalMode = 'e';
            }
            stream.str("");
            stream.clear();

            child = element->FirstChildElement("Appearance");
            stream << child->GetText() << std::endl;
            stream >> texture.appearance;
            stream.str("");
            stream.clear();

            textures.push_back(texture);

            element = element->NextSiblingElement("Texture");
        }
    }

    //Get Meshes
    element = root->FirstChildElement("Objects");
    element = element->FirstChildElement("Mesh");
    while (element) {
        Mesh mesh;
        child = element->FirstChildElement("Material");
        stream << child->GetText() << std::endl;
        stream >> mesh.material_id;

        child = element->FirstChildElement("Texture");
        stream << (child ? child->GetText() : "0") << std::endl;
        stream >> mesh.texture_id;
        stream.clear();

        child = element->FirstChildElement("Faces");
        stream << child->GetText() << std::endl;
        Face face;
        while (!(stream >> face.v0_id).eof()) {
            stream >> face.v1_id >> face.v2_id;
            if (mesh.texture_id) {
                face.texturePtr = &textures[mesh.texture_id - 1];
            } else {
                face.texturePtr = nullptr;
            }
            mesh.faces.push_back(face);
        }
        stream.clear();

        child = element->FirstChildElement("Transformations");
        if (child) {
            stream << child->GetText() << std::endl;
            char ttype;
            int id;
            while (!(stream >> ttype).eof()) {
                stream >> id;
                mesh.transformations.push_back(std::pair<char, int>(ttype, id));
            }
            stream.clear();
        }


        meshes.push_back(mesh);
        mesh.faces.clear();
        element = element->NextSiblingElement("Mesh");
    }
    stream.clear();

    //Get Triangles
    element = root->FirstChildElement("Objects");
    element = element->FirstChildElement("Triangle");
    while (element) {
        Triangle triangle;
        child = element->FirstChildElement("Material");
        stream << child->GetText() << std::endl;
        stream >> triangle.material_id;

        child = element->FirstChildElement("Indices");
        stream << child->GetText() << std::endl;
        stream >> triangle.indices.v0_id >> triangle.indices.v1_id >> triangle.indices.v2_id;

        child = element->FirstChildElement("Transformations");
        if (child) {
            stream << child->GetText() << std::endl;
            char ttype;
            int id;
            while (!(stream >> ttype).eof()) {
                stream >> id;
                triangle.transformations.push_back(std::pair<char, int>(ttype, id));
            }
            stream.clear();
        }

        child = element->FirstChildElement("Texture");
        stream << (child ? child->GetText() : "0") << std::endl;
        stream >> triangle.texture_id;
        triangle.texturePtr = nullptr;
        if (triangle.texture_id) {
            triangle.texturePtr = &textures[triangle.texture_id - 1];
        }
        stream.clear();

        triangles.push_back(triangle);
        element = element->NextSiblingElement("Triangle");
    }

    //Get Spheres
    element = root->FirstChildElement("Objects");
    element = element->FirstChildElement("Sphere");
    while (element) {
        Sphere sphere;
        child = element->FirstChildElement("Material");
        stream << child->GetText() << std::endl;
        stream >> sphere.material_id;

        child = element->FirstChildElement("Center");
        stream << child->GetText() << std::endl;
        stream >> sphere.center_vertex_id;

        child = element->FirstChildElement("Radius");
        stream << child->GetText() << std::endl;
        stream >> sphere.radius;

        child = element->FirstChildElement("Transformations");
        if (child) {
            stream << child->GetText() << std::endl;
            char ttype;
            int id;
            while (!(stream >> ttype).eof()) {
                stream >> id;
                sphere.transformations.push_back(std::pair<char, int>(ttype, id));
            }
            stream.clear();
        }

        child = element->FirstChildElement("Texture");
        stream << (child ? child->GetText() : "0") << std::endl;
        stream >> sphere.texture_id;
        sphere.texturePtr = nullptr;
        if (sphere.texture_id) {
            sphere.texturePtr = &textures[sphere.texture_id - 1];
        }
        stream.clear();

        spheres.push_back(sphere);
        element = element->NextSiblingElement("Sphere");
    }
    stream.clear();

    //Get MesheInstances
    element = root->FirstChildElement("Objects");
    element = element->FirstChildElement("MeshInstance");
    while (element) {
        MeshInstance meshInstance;
        stream << element->Attribute("baseMeshId") << std::endl;
        stream >> meshInstance.baseMesh_id;
        child = element->FirstChildElement("Material");
        stream << child->GetText() << std::endl;
        stream >> meshInstance.material_id;
        stream.clear();

        child = element->FirstChildElement("Texture");
        stream << (child ? child->GetText() : "0") << std::endl;
        stream >> meshInstance.texture_id;
        meshInstance.texturePtr = nullptr;
        if (meshInstance.texture_id) {
            meshInstance.texturePtr = &textures[meshInstance.texture_id - 1];
        }
        stream.clear();

        child = element->FirstChildElement("Transformations");
        if (child) {
            stream << child->GetText() << std::endl;
            char ttype;
            int id;
            while (!(stream >> ttype).eof()) {
                stream >> id;
                meshInstance.transformations.push_back(std::pair<char, int>(ttype, id));
            }
            stream.clear();
        }

        meshInstances.push_back(meshInstance);
        element = element->NextSiblingElement("MeshInstance");
    }
    stream.clear();

    //Get Transformations
    element = root->FirstChildElement("Transformations");
    if (element) {
        Scaling scaling;
        child = element->FirstChildElement("Scaling");
        while (child) {
            stream << child->GetText() << std::endl;
            stream >> scaling.scale.x >> scaling.scale.y >> scaling.scale.z;
            scaling.fillMatrix();
            transformations.scalings.push_back(scaling);
            child = child->NextSiblingElement("Scaling");
        }

        Rotation rotation;
        child = element->FirstChildElement("Rotation");
        while (child) {
            stream << child->GetText() << std::endl;
            stream >> rotation.angle >> rotation.u.x >> rotation.u.y >> rotation.u.z;
            rotation.fillMatrix();
            transformations.rotations.push_back(rotation);
            child = child->NextSiblingElement("Rotation");
        }

        Translation translation;
        child = element->FirstChildElement("Translation");
        while (child) {
            stream << child->GetText() << std::endl;
            stream >> translation.translate.x >> translation.translate.y >> translation.translate.z;
            translation.fillMatrix();
            transformations.translations.push_back(translation);
            child = child->NextSiblingElement("Translation");
        }

    }

    //Get TexCoordData
    element = root->FirstChildElement("TexCoordData");
    if (element) {
        stream << element->GetText() << std::endl;
        Vec3f coord;
        while (!(stream >> coord.x).eof()) {
            stream >> coord.y;
            tex_coord_data.push_back(coord);
        }
        stream.clear();
    }

}

void parser::Scaling::fillMatrix() {
    matrix.matrix4[0][0] = scale.x;
    matrix.matrix4[1][1] = scale.y;
    matrix.matrix4[2][2] = scale.z;
    matrix.matrix4[3][3] = 1;
}

void parser::Rotation::fillMatrix() {
    fst::math::Vector3f v, w;
    u = fst::math::normalize(u);
    if (abs(u.z) <= abs(u.y) && abs(u.z) <= abs(u.x)) {
        v.x = -u.y;
        v.y = u.x;
        v.z = 0;
    } else if (abs(u.y) <= abs(u.z) && abs(u.y) <= abs(u.x)) {
        v.x = -u.z;
        v.z = u.x;
        v.y = 0;
    } else if (abs(u.x) <= abs(u.z) && abs(u.x) <= abs(u.y)) {
        v.y = -u.z;
        v.z = u.y;
        v.x = 0;
    }

    v = fst::math::normalize(v);
    w = u % v;
    w = fst::math::normalize(w);

    fst::math::Matrix4f M;
    M.matrix4[0][0] = u.x;
    M.matrix4[0][1] = u.y;
    M.matrix4[0][2] = u.z;
    M.matrix4[1][0] = v.x;
    M.matrix4[1][1] = v.y;
    M.matrix4[1][2] = v.z;
    M.matrix4[2][0] = w.x;
    M.matrix4[2][1] = w.y;
    M.matrix4[2][2] = w.z;
    M.matrix4[3][3] = 1;

//    fst::math::Matrix4f MT;
//    MT.matrix4[0][0] = u.x;
//    MT.matrix4[0][1] = v.x;
//    MT.matrix4[0][2] = w.x;
//    MT.matrix4[1][0] = u.y;
//    MT.matrix4[1][1] = v.y;
//    MT.matrix4[1][2] = w.y;
//    MT.matrix4[2][0] = u.z;
//    MT.matrix4[2][1] = v.z;
//    MT.matrix4[2][2] = w.z;
//    MT.matrix4[3][3] = 1;

    fst::math::Matrix4f R;
    float radian = static_cast<float>(angle * PI / 180);
    float cs = static_cast<float>(cos(radian));
    float sn = static_cast<float>(sin(radian));
    R.matrix4[0][0] = 1;
    R.matrix4[1][1] = cs;
    R.matrix4[1][2] = -sn;
    R.matrix4[2][1] = sn;
    R.matrix4[2][2] = cs;
    R.matrix4[3][3] = 1;


    matrix = fst::math::transpose(M) * R * M;
}

void parser::Translation::fillMatrix() {
    matrix.matrix4[0][0] = 1;
    matrix.matrix4[0][3] = translate.x;
    matrix.matrix4[1][1] = 1;
    matrix.matrix4[1][3] = translate.y;
    matrix.matrix4[2][2] = 1;
    matrix.matrix4[2][3] = translate.z;
    matrix.matrix4[3][3] = 1;
}

parser::Vec3f toVec3f(fst::math::Vector4f v) {
//    return parser::Vec3f{v.x, v.y, v.z};
    return parser::Vec3f{v.x / v.w, v.y / v.w, v.z / v.w};
}

fst::math::Vector4f toVector4f(parser::Vec3f v) {
    return fst::math::Vector4f(v.x, v.y, v.z, 1);
}

fst::math::Vector4f operator*(const fst::math::Matrix4f m, const fst::math::Vector4f &point) {
    fst::math::Vector4f result{};
    float *f = &(result.x);
    for (int i = 0; i < 4; ++i) {
        f[i] =
                m.matrix4[i][0] * point.x +
                m.matrix4[i][1] * point.y +
                m.matrix4[i][2] * point.z +
                m.matrix4[i][3] * point.w;
    }
    return result;
}

void parser::Scene::faceTransform(parser::Face &face, std::vector<std::pair<char, int>> &faceTransformations,
                                  unsigned int &vertexDataSize) {
    fst::math::Matrix4f tm;
    fst::math::Matrix4f *tmp = nullptr;

    tm.toIdentity();
    for (auto &transformation: faceTransformations) {
        if ('t' == transformation.first) {
            tmp = &(transformations.translations[transformation.second - 1].matrix);
        } else if ('r' == transformation.first) {
            tmp = &(transformations.rotations[transformation.second - 1].matrix);
        } else if ('s' == transformation.first) {
            tmp = &(transformations.scalings[transformation.second - 1].matrix);
        } else {
            std::cout << "ERROR: transformation " << transformation.second << std::endl;
        }
        tm = *tmp * tm;
    }
    fst::math::Vector4f tmp4f{};
    tmp4f = tm * toVector4f(vertex_data[face.v0_id - 1]);
    vertex_data.push_back(toVec3f(tmp4f));
    vertexDataSize++;
    face.v0_id = vertexDataSize;

    tmp4f = tm * toVector4f(vertex_data[face.v1_id - 1]);
    vertex_data.push_back(toVec3f(tmp4f));
    vertexDataSize++;
    face.v1_id = vertexDataSize;

    tmp4f = tm * toVector4f(vertex_data[face.v2_id - 1]);
    vertex_data.push_back(toVec3f(tmp4f));
    vertexDataSize++;
    face.v2_id = vertexDataSize;
}

void parser::Scene::triangleTransformation() {
    auto vertexDataSize = static_cast<unsigned int>(vertex_data.size());
    for (auto &triangle: triangles) {
        if (triangle.texture_id) {
            triangle.indices.texCoord0 = tex_coord_data[triangle.indices.v0_id - 1];
            triangle.indices.texCoord1 = tex_coord_data[triangle.indices.v1_id - 1];
            triangle.indices.texCoord2 = tex_coord_data[triangle.indices.v2_id - 1];
        }
        faceTransform(triangle.indices, triangle.transformations, vertexDataSize);
    }
};

void parser::Scene::meshTransformation() {
    auto vertexDataSize = static_cast<unsigned int>(vertex_data.size());
    for (auto &mesh: meshes) {
        for (auto &face: mesh.faces) {
            if (mesh.texture_id) {
                face.texCoord0 = tex_coord_data[face.v0_id - 1];
                face.texCoord1 = tex_coord_data[face.v1_id - 1];
                face.texCoord2 = tex_coord_data[face.v2_id - 1];
            }
            faceTransform(face, mesh.transformations, vertexDataSize);
        }
    }
}

void parser::Scene::meshInstancesTransformation() {
    Mesh baseMesh;
    auto vertexDataSize = static_cast<unsigned int>(vertex_data.size());
    for (auto &meshInstance: meshInstances) {
        baseMesh = meshes[meshInstance.baseMesh_id - 1];
        std::vector<Face> newFaces = baseMesh.faces;
        Mesh newMesh{meshInstance.transformations, meshInstance.material_id, newFaces, meshInstance.texture_id,
                     meshInstance.texturePtr};
        for (auto &face: newMesh.faces) {
            faceTransform(face, meshInstance.transformations, vertexDataSize);
        }
        meshes.push_back(newMesh);
    }
}

void parser::Scene::sphereTransformation(Sphere &sphere, std::vector<std::pair<char, int>> &sphereTransformations,
                                         unsigned int &vertexDataSize) {
    fst::math::Matrix4f tm;
    fst::math::Matrix4f *tmp = nullptr;
    fst::math::Vector4f tmp4f{};

    tmp4f = toVector4f(vertex_data[sphere.center_vertex_id - 1]);
    tm.toIdentity();
    for (auto &transformation: sphereTransformations) {
        if ('t' == transformation.first) {
            tmp = &(transformations.translations[transformation.second - 1].matrix);
        } else if ('r' == transformation.first) {
            tmp = &(transformations.rotations[transformation.second - 1].matrix);
        } else if ('s' == transformation.first) {
            tmp = &(transformations.scalings[transformation.second - 1].matrix);
            if (tmp->matrix4[0][0] == tmp->matrix4[1][1] && tmp->matrix4[1][1] == tmp->matrix4[2][2]) {
                if(vertex_data[sphere.center_vertex_id - 1].x == 0 &&
                        vertex_data[sphere.center_vertex_id - 1].y == 0 &&
                        vertex_data[sphere.center_vertex_id - 1].z == 0 )
                sphere.radius *= tmp->matrix4[1][1];
            }
        } else {
            std::cout << "ERROR: transformation " << transformation.second << std::endl;
        }
        tm = *tmp * tm;
    }
    tmp4f = tm * toVector4f(vertex_data[sphere.center_vertex_id - 1]);
    sphere.center = toVec3f(tmp4f);
    vertex_data.push_back(sphere.center);
    vertexDataSize++;
    sphere.center_vertex_id = vertexDataSize;
}

void parser::Scene::spheresTransformations() {
    auto vertexDataSize = static_cast<unsigned int>(vertex_data.size());
    for (auto &sphere: spheres) {
        sphereTransformation(sphere, sphere.transformations, vertexDataSize);
    }
}
