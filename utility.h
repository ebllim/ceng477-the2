#ifndef __UTILITY_H__
#define __UTILITY_H__

namespace fst
{
    namespace math
    {
        static float min(float a, float b)
        {
            return a < b ? a : b;
        }

        static float max(float a, float b)
        {
            return a > b ? a : b;
        }
    }
}
#endif