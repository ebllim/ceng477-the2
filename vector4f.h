#ifndef __VECTOR4F_H__
#define __VECTOR4F_H__

#include "parser.h"

namespace fst {
    namespace math {
        struct Vector4f {
            float x, y, z, w;

            Vector4f() = default;

            explicit Vector4f(float a)
                    : x(a), y(a), z(a), w(a) {}

            Vector4f(float a, float b, float c, float d)
                    : x(a), y(b), z(c), w(d) {}

            Vector4f operator = (Vector4f v){
                this->x = v.x;
                this->y = v.y;
                this->z = v.z;
                this->w = v.w;
            }
        };

    }
}
#endif