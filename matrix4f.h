#ifndef __MATRIX4F_H__
#define __MATRIX4F_H__

#include <iostream>

namespace fst {
    namespace math {
        class Matrix4f {

        public:
            float matrix4[4][4];

            Matrix4f() {
                for (auto &i : this->matrix4) {
                    i[0] = 0;
                    i[1] = 0;
                    i[2] = 0;
                    i[3] = 0;
                }
            }

            void printMatrix(char *title) {
                if (title)
                    std::cout << title << std::endl;
                for (auto &i : this->matrix4) {
                    std::cout << i[0] << " " << i[1] << " " << i[2] << " " << i[3] << std::endl;
                }
            }

            Matrix4f operator*(const Matrix4f &m) const {

                Matrix4f a;
                for (int i = 0; i < 4; ++i) {
                    for (int j = 0; j < 4; ++j) {
                        a.matrix4[i][j] =
                                this->matrix4[i][0] * m.matrix4[0][j] +
                                this->matrix4[i][1] * m.matrix4[1][j] +
                                this->matrix4[i][2] * m.matrix4[2][j] +
                                this->matrix4[i][3] * m.matrix4[3][j];
                    }
                }
                return a;
            }

            void toIdentity() {
                for (int j = 0; j < 4; ++j) {
                    for (int i = 0; i < 4; ++i) {
                        this->matrix4[j][i] = 0;
                    }

                }
//                for (auto &i : this->matrix4) {
//                    i[0] = 0;
//                    i[1] = 0;
//                    i[2] = 0;
//                    i[3] = 0;
//                }
                this->matrix4[0][0] = 1;
                this->matrix4[1][1] = 1;
                this->matrix4[2][2] = 1;
                this->matrix4[3][3] = 1;
            }


        };

        Matrix4f static transpose(Matrix4f base) {
            Matrix4f a;
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    a.matrix4[i][j] = base.matrix4[j][i];
                }
            }
            return a;
        }


    }
}
#endif
