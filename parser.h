#ifndef __HW1__PARSER__
#define __HW1__PARSER__
#define PI 3.1415926535897932384

#include <string>
#include <vector>
#include "matrix4f.h"
#include "vector3f.h"
#include "vector4f.h"

namespace parser {
    //Notice that all the structures are as simple as possible
    //so that you are not enforced to adopt any style or design.
    struct Vec3f {
        float x, y, z;


    };

    struct Vec3i {
        int x, y, z;
    };

    struct Vec4f {
        float x, y, z, w;
    };


    struct Camera {
        Vec3f position;
        Vec3f gaze;
        Vec3f up;
        Vec4f near_plane;
        float near_distance;
        int image_width, image_height;
        std::string image_name;
    };

    struct PointLight {
        Vec3f position;
        Vec3f intensity;
    };

    struct Material {
        Vec3f ambient;
        Vec3f diffuse;
        Vec3f specular;
        Vec3f mirror;
        float phong_exponent;
    };

    struct Texture {
        std::string image_name;
        char interpolation;
        char decalMode;
        char appearance;
        unsigned char * textureImg;
        int nx;
        int ny;

    };

    struct Face {
        int v0_id;
        int v1_id;
        int v2_id;
        Vec3f texCoord0;
        Vec3f texCoord1;
        Vec3f texCoord2;
        Texture* texturePtr;
    };

    struct Mesh {
        std::vector<std::pair<char, int>> transformations;
        int material_id;
        std::vector<Face> faces;
        int texture_id;
        Texture* texturePtr;
    };

    struct Triangle {
        std::vector<std::pair<char, int>> transformations;
        int material_id;
        Face indices;
        int texture_id;
        Texture* texturePtr;
    };

    struct Sphere {
        std::vector<std::pair<char, int>> transformations;
        int material_id;
        int center_vertex_id;
        float radius;
        int texture_id;
        Vec3f center;
        Texture* texturePtr;
    };

    struct MeshInstance {
        std::vector<std::pair<char, int>> transformations;
        int material_id;
        int texture_id;
        int baseMesh_id;
        Texture* texturePtr;
    };


    struct Scaling {
//        int id;
        fst::math::Vector3f scale;
        fst::math::Matrix4f matrix;

        void fillMatrix();
    };

    struct Rotation {
//        int id;
        float angle;
        fst::math::Vector3f u;
        fst::math::Matrix4f matrix;

        void fillMatrix();
    };

    struct Translation {
//        int id;
        fst::math::Vector3f translate;
        fst::math::Matrix4f matrix;

        void fillMatrix();
    };

    struct Transformations {
        std::vector<Scaling> scalings;
        std::vector<Rotation> rotations;
        std::vector<Translation> translations;

    };

    struct Scene {
        //Data
        Vec3i background_color;
        float shadow_ray_epsilon;
        int max_recursion_depth;
        std::vector<Camera> cameras;
        Vec3f ambient_light;
        std::vector<PointLight> point_lights;
        std::vector<Material> materials;
        std::vector<Vec3f> vertex_data;
        std::vector<Vec3f> tex_coord_data;
        std::vector<Mesh> meshes;
        std::vector<Triangle> triangles;
        std::vector<Sphere> spheres;
        std::vector<MeshInstance> meshInstances;

        Transformations transformations;
        std::vector<Texture> textures;

        //Functions
        void loadFromXml(const std::string &filepath);

        void triangleTransformation();

        void meshTransformation();

        void faceTransform(parser::Face &face, std::vector<std::pair<char, int>> &faceTransformations,
                           unsigned int &vertexDataSize);

        void meshInstancesTransformation();

        void sphereTransformation(parser::Sphere &sphere,
                                                 std::vector<std::pair<char, int>> &sphereTransformations,
                                                 unsigned int &vertexDataSize);
        void spheresTransformations();
    };
}

#endif