#ifndef RAYTRACER_TEXTURE_H
#define RAYTRACER_TEXTURE_H

#include <vector>
#include "jpeg.h"
#include "parser.h"
#include "hit_record.h"

void loadTextureJpegs(std::vector<unsigned char *> &textureJpgs, std::vector<parser::Texture> &textures);

class Texture {
public:
    Texture();
    fst::math::Vector3f textureCalculator(fst::HitRecord &hit_record);
};

#endif //RAYTRACER_TEXTURE_H
